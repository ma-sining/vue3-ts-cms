# vue3-ts-cms

>学习coderwhy的vue3后台管理项目


## Project setup

```shell
# 安装依赖 
yarn install
# 启动服务
yarn serve
# 打包
yarn build
# 检查代码中的编写规范
yarn lint
```
```
├─框架实现
│  ├─APP开发框架搭建
│  ├─登录对接
│  ├─TOKEN接口机制
│  ├─热更新\覆盖更新
├─基础功能
```


### 预览图

![查询商品](./src/assets/doc/dashboard.png)
