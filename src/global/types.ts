export interface FormatTimeFunction {
  (value: string): string
}
