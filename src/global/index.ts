import { App } from 'vue'
import registerProperties from './register-properties'

export * from './types'

// 全局注册
export function globalRegister(app: App): void {
  app.use(registerProperties)
}
