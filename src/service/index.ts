// service统一出口
import MARequest from './request'
import { BASE_URL, TIME_OUT } from './request/config'
import localCache from '@/utils/cache'

import { AxiosRequestHeaders } from 'axios'

const maRequest = new MARequest({
  baseURL: BASE_URL,
  timeout: TIME_OUT,
  headers: {} as AxiosRequestHeaders,
  //实例的拦截
  interceptors: {
    requestInterceptor: (config) => {
      const token = localCache.getCache('token')
      if (token) {
        config.headers.Authorization = `Bearer ${token}`
      }
      return config
    },
    requestInterceptorCatch: (error) => {
      return error
    },
    responseInterceptor: (config) => {
      return config
    },
    responseInterceptorCatch: (error) => {
      return error
    }
  }
})

export default maRequest
