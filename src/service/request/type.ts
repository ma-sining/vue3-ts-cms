import type { InternalAxiosRequestConfig, AxiosResponse } from 'axios'

export interface MARequestInterceptors<T = AxiosResponse> {
  requestInterceptor?: (
    config: InternalAxiosRequestConfig
  ) => InternalAxiosRequestConfig
  requestInterceptorCatch?: (error: any) => any
  responseInterceptor?: (res: T) => T
  responseInterceptorCatch?: (error: any) => any
}
// 为了扩展类型(如：interceptors)
export interface MARequestConfig<T = AxiosResponse>
  extends InternalAxiosRequestConfig {
  interceptors?: MARequestInterceptors<T>
  showLoading?: boolean
}
