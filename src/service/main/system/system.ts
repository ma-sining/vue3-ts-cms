import { AxiosRequestHeaders } from 'axios'
import hyRequest from '../../index'

import { IDataType } from '../../types'

export function getPageListData(url: string, queryInfo: any) {
  return hyRequest.post<IDataType>({
    url: url,
    data: queryInfo,
    headers: {} as AxiosRequestHeaders
  })
}

// url: /users/id
export function deletePageData(url: string) {
  return hyRequest.delete<IDataType>({
    url: url,
    headers: {} as AxiosRequestHeaders
  })
}

export function createPageData(url: string, newData: any) {
  return hyRequest.post<IDataType>({
    url: url,
    data: newData,
    headers: {} as AxiosRequestHeaders
  })
}

export function editPageData(url: string, editData: any) {
  return hyRequest.patch<IDataType>({
    url: url,
    data: editData,
    headers: {} as AxiosRequestHeaders
  })
}
