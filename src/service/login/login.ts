import hyRequest from '../index'
import { AxiosRequestHeaders } from 'axios'

import { IAccount, ILoginResult } from './type'
import { IDataType } from '../types'

enum LoginAPI {
  AccountLogin = '/login',
  LoginUserInfo = '/users/', // 用法: /users/1
  UserMenus = '/role/' // 用法: role/1/menu
}

// 用户登录
export function accountLoginRequest(account: IAccount) {
  return hyRequest.post<IDataType<ILoginResult>>({
    url: LoginAPI.AccountLogin,
    headers: {} as AxiosRequestHeaders,
    data: account
  })
}

// 请求某个用户信息
export function requestUserInfoById(id: number) {
  return hyRequest.get<IDataType>({
    url: LoginAPI.LoginUserInfo + id,
    headers: {} as AxiosRequestHeaders,
    showLoading: false
  })
}

// 获取某个用户的菜单
export function requestUserMenusByRoleId(id: number) {
  return hyRequest.get<IDataType>({
    url: LoginAPI.UserMenus + id + '/menu',
    headers: {} as AxiosRequestHeaders,
    showLoading: false
  })
}
