export const userData = {
  list: [
    {
      id: 45,
      name: 'james',
      realname: '詹姆斯',
      cellphone: '12290554444',
      enable: 1,
      departmentId: 1,
      roleId: 1,
      createAt: '2021-08-08T02:59:13.000Z',
      updateAt: '2021-08-08T02:59:13.000Z'
    },
    {
      id: 42,
      name: 'jerry',
      realname: 'jerry',
      cellphone: '13899999999',
      enable: 0,
      departmentId: 1,
      roleId: 1,
      createAt: '2021-07-01T02:59:13.000Z',
      updateAt: '2021-07-22T02:59:13.000Z'
    }
  ],
  totalCount: 2
}

export const roleData = {
  list: [
    {
      id: 1,
      name: '超级管理员',
      intro: '所有权限',
      createAt: '2021-08-08T02:59:13.000Z',
      updateAt: '2021-08-08T02:59:13.000Z',
      menuList: [
        {
          icon: 'Monitor',
          id: 1,
          name: '系统总览',
          sort: 1,
          type: 1,
          url: '/main/analysis',
          children: [
            {
              id: 5,
              url: '/main/analysis/overview',
              name: '核心技术',
              sort: 106,
              type: 2,
              children: null,
              parentId: 38
            },
            {
              id: 6,
              url: '/main/analysis/dashboard',
              name: '商品统计',
              sort: 107,
              type: 2,
              children: null,
              parentId: 38
            }
          ]
        }
      ]
    },
    {
      id: 2,
      name: '运营',
      intro: '日常事务',
      createAt: '2021-08-08T02:59:13.000Z',
      updateAt: '2021-08-08T02:59:13.000Z',
      menuList: [
        {
          icon: 'Monitor',
          id: 1,
          name: '系统总览',
          sort: 1,
          type: 1,
          url: '/main/analysis',
          children: [
            {
              id: 5,
              url: '/main/analysis/overview',
              name: '核心技术',
              sort: 106,
              type: 2,
              children: null,
              parentId: 38
            },
            {
              id: 6,
              url: '/main/analysis/dashboard',
              name: '商品统计',
              sort: 107,
              type: 2,
              children: null,
              parentId: 38
            }
          ]
        }
      ]
    },
    {
      id: 3,
      name: '人事',
      intro: '人事管理',
      createAt: '2021-08-08T02:59:13.000Z',
      updateAt: '2021-08-08T02:59:13.000Z',
      menuList: [
        {
          icon: 'Monitor',
          id: 1,
          name: '系统总览',
          sort: 1,
          type: 1,
          url: '/main/analysis',
          children: [
            {
              id: 5,
              url: '/main/analysis/overview',
              name: '核心技术',
              sort: 106,
              type: 2,
              children: null,
              parentId: 38
            },
            {
              id: 6,
              url: '/main/analysis/dashboard',
              name: '商品统计',
              sort: 107,
              type: 2,
              children: null,
              parentId: 38
            }
          ]
        }
      ]
    }
  ],
  totalCount: 3
}

export const goodsData = {
  list: [
    {
      id: 9,
      name: '秋装女2018新款早秋女装裙子韩版针织连衣裙+衬衫上衣时尚套装',
      oldPrice: '127',
      newPrice: '89',
      desc: '秋装女2018新款早秋女装裙子韩版针织连衣裙+衬衫上衣时尚套装',
      status: 1,
      imgUrl:
        'https://fuss10.elemecdn.com/a/3f/3302e58f9a181d2509f3dc0fa68b0jpeg.jpeg',
      inventoryCount: 4044,
      saleCount: 32070,
      favorCount: 179,
      address: '上海',
      categoryId: 7,
      createAt: '2021-08-08T02:59:13.000Z',
      updateAt: '2021-08-08T02:59:13.000Z'
    }
  ],
  totalCount: 1
}

export const menuData = {
  list: [
    {
      id: 38,
      name: '系统总览',
      type: 1,
      url: '/main/analysis',
      icon: 'Monitor',
      sort: 1,
      createAt: '2021-04-19T02:59:13.000Z',
      updateAt: '2021-04-30T02:59:13.000Z',
      children: [
        {
          id: 39,
          url: '/main/analysis/overview',
          name: '核心技术',
          sort: 106,
          type: 2,
          children: null,
          parentId: 38,
          createAt: '2021-01-02T02:59:13.000Z',
          updateAt: '2021-04-27T02:59:13.000Z'
        },
        {
          id: 40,
          url: '/main/analysis/dashboard',
          name: '商品统计',
          sort: 107,
          type: 2,
          children: null,
          parentId: 38,
          createAt: '2021-01-02T02:59:13.000Z',
          updateAt: '2021-04-27T02:59:13.000Z'
        }
      ]
    },
    {
      icon: 'Setting',
      id: 1,
      name: '系统管理',
      sort: 2,
      type: 1,
      url: '/main/system',
      createAt: '2021-04-19T02:59:13.000Z',
      updateAt: '2021-04-30T02:59:13.000Z',
      children: [
        {
          id: 2,
          url: '/main/system/user',
          name: '用户管理',
          sort: 100,
          type: 2,
          createAt: '2021-04-19T02:59:13.000Z',
          updateAt: '2021-04-30T02:59:13.000Z',
          children: [
            {
              id: 5,
              url: null,
              name: '创建用户',
              sort: null,
              type: 3,
              parentId: 2,
              permission: 'system:users:create',
              createAt: '2021-04-19T02:59:13.000Z',
              updateAt: '2021-04-30T02:59:13.000Z'
            },
            {
              id: 6,
              url: null,
              name: '删除用户',
              sort: null,
              type: 3,
              parentId: 2,
              permission: 'system:users:delete',
              createAt: '2021-04-19T02:59:13.000Z',
              updateAt: '2021-04-30T02:59:13.000Z'
            },
            {
              id: 7,
              url: null,
              name: '修改用户',
              sort: null,
              type: 3,
              parentId: 2,
              permission: 'system:users:edit',
              createAt: '2021-04-19T02:59:13.000Z',
              updateAt: '2021-04-30T02:59:13.000Z'
            },
            {
              id: 8,
              url: null,
              name: '查询用户',
              sort: null,
              type: 3,
              parentId: 2,
              permission: 'system:users:query',
              createAt: '2021-04-19T02:59:13.000Z',
              updateAt: '2021-04-30T02:59:13.000Z'
            }
          ]
        },
        {
          id: 3,
          url: '/main/system/department',
          name: '部门管理',
          sort: 101,
          type: 2,
          createAt: '2021-04-19T02:59:13.000Z',
          updateAt: '2021-04-30T02:59:13.000Z',
          children: [
            {
              id: 17,
              url: null,
              name: '创建部门',
              sort: null,
              type: 3,
              parentId: 3,
              permission: 'system:department:create',
              createAt: '2021-04-19T02:59:13.000Z',
              updateAt: '2021-04-30T02:59:13.000Z'
            },
            {
              id: 18,
              url: null,
              name: '删除部门',
              sort: null,
              type: 3,
              parentId: 3,
              permission: 'system:department:delete',
              createAt: '2021-04-19T02:59:13.000Z',
              updateAt: '2021-04-30T02:59:13.000Z'
            },
            {
              id: 19,
              url: null,
              name: '修改部门',
              sort: null,
              type: 3,
              parentId: 3,
              permission: 'system:department:edit',
              createAt: '2021-04-19T02:59:13.000Z',
              updateAt: '2021-04-30T02:59:13.000Z'
            },
            {
              id: 20,
              url: null,
              name: '查询部门',
              sort: null,
              type: 3,
              parentId: 3,
              permission: 'system:department:query',
              createAt: '2021-04-19T02:59:13.000Z',
              updateAt: '2021-04-30T02:59:13.000Z'
            }
          ]
        },
        {
          id: 4,
          url: '/main/system/menu',
          name: '菜单管理',
          sort: 102,
          type: 2,
          createAt: '2021-04-19T02:59:13.000Z',
          updateAt: '2021-04-30T02:59:13.000Z',
          children: [
            {
              id: 9,
              url: null,
              name: '创建菜单',
              sort: null,
              type: 3,
              parentId: 4,
              permission: 'system:menu:create',
              createAt: '2021-04-19T02:59:13.000Z',
              updateAt: '2021-04-30T02:59:13.000Z'
            },
            {
              id: 10,
              url: null,
              name: '删除菜单',
              sort: null,
              type: 3,
              parentId: 4,
              permission: 'system:menu:delete',
              createAt: '2021-04-19T02:59:13.000Z',
              updateAt: '2021-04-30T02:59:13.000Z'
            },
            {
              id: 11,
              url: null,
              name: '修改菜单',
              sort: null,
              type: 3,
              parentId: 4,
              permission: 'system:menu:edit',
              createAt: '2021-04-19T02:59:13.000Z',
              updateAt: '2021-04-30T02:59:13.000Z'
            },
            {
              id: 12,
              url: null,
              name: '查询菜单',
              sort: null,
              type: 3,
              parentId: 4,
              permission: 'system:menu:query',
              createAt: '2021-04-19T02:59:13.000Z',
              updateAt: '2021-04-30T02:59:13.000Z'
            }
          ]
        },
        {
          id: 5,
          url: '/main/system/role',
          name: '角色管理',
          sort: 101,
          type: 2,
          createAt: '2021-04-19T02:59:13.000Z',
          updateAt: '2021-04-30T02:59:13.000Z',
          children: [
            {
              id: 12,
              url: null,
              name: '创建角色',
              sort: null,
              type: 3,
              parentId: 5,
              permission: 'system:role:create',
              createAt: '2021-04-19T02:59:13.000Z',
              updateAt: '2021-04-30T02:59:13.000Z'
            },
            {
              id: 13,
              url: null,
              name: '删除角色',
              sort: null,
              type: 3,
              parentId: 5,
              permission: 'system:role:delete',
              createAt: '2021-04-19T02:59:13.000Z',
              updateAt: '2021-04-30T02:59:13.000Z'
            },
            {
              id: 14,
              url: null,
              name: '修改角色',
              sort: null,
              type: 3,
              parentId: 5,
              permission: 'system:role:edit',
              createAt: '2021-04-19T02:59:13.000Z',
              updateAt: '2021-04-30T02:59:13.000Z'
            },
            {
              id: 14,
              url: null,
              name: '查询角色',
              sort: null,
              type: 3,
              parentId: 5,
              permission: 'system:role:query',
              createAt: '2021-04-19T02:59:13.000Z',
              updateAt: '2021-04-30T02:59:13.000Z'
            }
          ]
        }
      ]
    },
    {
      icon: 'Handbag',
      id: 9,
      name: '商品中心',
      sort: 3,
      type: 1,
      url: '/main/product',
      createAt: '2021-04-19T02:59:13.000Z',
      updateAt: '2021-04-30T02:59:13.000Z',
      children: [
        {
          id: 10,
          url: '/main/product/category',
          name: '商品类别',
          sort: 20,
          type: 2,
          children: null,
          parentId: 9,
          createAt: '2021-04-19T02:59:13.000Z',
          updateAt: '2021-04-30T02:59:13.000Z'
        },
        {
          id: 11,
          url: '/main/product/goods',
          name: '商品信息',
          sort: 21,
          type: 2,
          children: null,
          parentId: 9,
          createAt: '2021-04-19T02:59:13.000Z',
          updateAt: '2021-04-30T02:59:13.000Z'
        }
      ]
    },
    {
      icon: 'ChatLineRound',
      id: 70,
      name: '随便聊聊',
      sort: 5,
      type: 1,
      url: '/main/story',
      createAt: '2021-04-19T02:59:13.000Z',
      updateAt: '2021-04-30T02:59:13.000Z',
      children: [
        {
          id: 71,
          url: '/main/story/chat',
          name: '你的故事',
          sort: 30,
          type: 2,
          children: null,
          parentId: 70,
          createAt: '2021-04-19T02:59:13.000Z',
          updateAt: '2021-04-30T02:59:13.000Z'
        },
        {
          id: 72,
          url: '/main/story/list',
          name: '故事列表',
          sort: 31,
          type: 2,
          children: null,
          parentId: 70,
          createAt: '2021-04-19T02:59:13.000Z',
          updateAt: '2021-04-30T02:59:13.000Z'
        }
      ]
    }
  ],
  totalCount: 4
}
