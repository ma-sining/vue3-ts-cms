// 每个分类商品的个数
export const goodsCategoryCount = {
  data: [
    {
      id: 2,
      name: '上衣',
      goodsCount: 14
    },
    {
      id: 3,
      name: '裤子',
      goodsCount: 20
    },
    {
      id: 4,
      name: '鞋子',
      goodsCount: 19
    },
    {
      id: 5,
      name: '厨具',
      goodsCount: 14
    },
    {
      id: 6,
      name: '家具',
      goodsCount: 18
    },
    {
      id: 7,
      name: '床上用品',
      goodsCount: 19
    },
    {
      id: 8,
      name: '女装',
      goodsCount: 35
    }
  ]
}

// 每个分类商品的销量
export const goodsCategorySale = {
  data: [
    {
      id: 2,
      name: '上衣',
      goodsCount: 49749
    },
    {
      id: 3,
      name: '裤子',
      goodsCount: 84813
    },
    {
      id: 4,
      name: '鞋子',
      goodsCount: 58345
    },
    {
      id: 5,
      name: '厨具',
      goodsCount: 57345
    },
    {
      id: 6,
      name: '家具',
      goodsCount: 14864
    },
    {
      id: 7,
      name: '床上用品',
      goodsCount: 37367
    },
    {
      id: 8,
      name: '女装',
      goodsCount: 73483
    }
  ]
}

// 每个分类商品的收藏
export const goodsCategoryFavor = {
  data: [
    {
      id: 2,
      name: '上衣',
      goodsFavor: 6091
    },
    {
      id: 3,
      name: '裤子',
      goodsFavor: 5251
    },
    {
      id: 4,
      name: '鞋子',
      goodsFavor: 19546
    },
    {
      id: 5,
      name: '厨具',
      goodsFavor: 13854
    },
    {
      id: 6,
      name: '家具',
      goodsFavor: 16732
    },
    {
      id: 7,
      name: '床上用品',
      goodsFavor: 4234
    },
    {
      id: 8,
      name: '女装',
      goodsFavor: 16859
    }
  ]
}

// 每个分类商品的收藏
export const goodsAddressSale = {
  data: [
    {
      address: '上海',
      count: 66378
    },
    {
      address: '南京',
      count: 55347
    },
    {
      address: '郑州',
      count: 53786
    },
    {
      address: '广州',
      count: 6637
    },
    {
      address: '长沙',
      count: 4637
    },
    {
      address: '武汉',
      count: 55876
    }
  ]
}

// 商品数量统计的数量
export const goodsAmountList = {
  data: [
    {
      amount: 'sale',
      title: '商品总销量',
      tips: '所有商品的总销量',
      subtittle: '商品总销量',
      number1: 510173,
      number2: 510173
    },
    {
      amount: 'favor',
      title: '商品总收藏',
      tips: '所有商品的总收藏',
      subtittle: '商品总收藏',
      number1: 87606,
      number2: 87606
    },
    {
      amount: 'inventory',
      title: '商品总库存',
      tips: '所有商品的总库存',
      subtittle: '商品总库存',
      number1: 867271,
      number2: 867271
    },
    {
      amount: 'saleroom',
      title: '商品总销售额',
      tips: '所有商品的总销售额',
      subtittle: '商品总销售额',
      number1: 43130820,
      number2: 43130820
    }
  ],
  totalCount: 4
}
