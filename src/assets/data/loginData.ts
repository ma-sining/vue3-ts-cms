export const userData = {
  id: 1,
  name: 'xiaoma',
  password: '123456',
  token: 'ksdjnosadjfoajdqwekdalkjf'
}

export const userInfo = {
  id: 1,
  name: 'xiaoma',
  realname: 'xiaoma',
  cellphone: 18812345678,
  enable: 1,
  createAt: '2023-06-09',
  updateAt: '2023-06-09',
  role: {
    id: 1,
    name: '超级管理员',
    intro: '所有权限',
    createAt: '2023-06-09',
    updateAt: '2023-06-09'
  },
  department: {
    id: 1,
    name: '总裁办',
    parentId: null,
    createAt: '2023-06-09',
    updateAt: '2023-06-09',
    leader: '小马'
  }
}

export const userMenus = [
  {
    children: [
      {
        id: 5,
        url: '/main/analysis/overview',
        name: '核心技术',
        sort: 106,
        type: 2,
        children: null,
        parentId: 38
      },
      {
        id: 6,
        url: '/main/analysis/dashboard',
        name: '商品统计',
        sort: 107,
        type: 2,
        children: null,
        parentId: 38
      }
    ],
    icon: 'Monitor',
    id: 1,
    name: '系统总览',
    sort: 1,
    type: 1,
    url: '/main/analysis'
  },
  {
    icon: 'Setting',
    id: 2,
    name: '系统管理',
    sort: 2,
    type: 1,
    url: '/main/system',
    children: [
      {
        id: 7,
        url: '/main/system/user',
        name: '用户管理',
        sort: 100,
        type: 2,
        children: [
          {
            id: 15,
            url: null,
            name: '创建用户',
            sort: null,
            type: 3,
            parentId: 2,
            permission: 'system:users:create'
          },
          {
            id: 16,
            url: null,
            name: '删除用户',
            sort: null,
            type: 3,
            parentId: 2,
            permission: 'system:users:delete'
          },
          {
            id: 17,
            url: null,
            name: '修改用户',
            sort: null,
            type: 3,
            parentId: 2,
            permission: 'system:users:edit'
          },
          {
            id: 18,
            url: null,
            name: '查询用户',
            sort: null,
            type: 3,
            parentId: 2,
            permission: 'system:users:query'
          }
        ]
      },
      {
        id: 8,
        url: '/main/system/department',
        name: '部门管理',
        sort: 101,
        type: 2,
        children: [
          {
            id: 18,
            url: null,
            name: '创建部门',
            sort: null,
            type: 3,
            parentId: 3,
            permission: 'system:department:create'
          },
          {
            id: 20,
            url: null,
            name: '删除部门',
            sort: null,
            type: 3,
            parentId: 3,
            permission: 'system:department:delete'
          },
          {
            id: 21,
            url: null,
            name: '修改部门',
            sort: null,
            type: 3,
            parentId: 3,
            permission: 'system:department:edit'
          },
          {
            id: 22,
            url: null,
            name: '查询部门',
            sort: null,
            type: 3,
            parentId: 3,
            permission: 'system:department:query'
          }
        ]
      },
      {
        id: 9,
        url: '/main/system/menu',
        name: '菜单管理',
        sort: 102,
        type: 2,
        children: [
          {
            id: 23,
            url: null,
            name: '创建菜单',
            sort: null,
            type: 3,
            parentId: 4,
            permission: 'system:menu:create'
          },
          {
            id: 24,
            url: null,
            name: '删除菜单',
            sort: null,
            type: 3,
            parentId: 4,
            permission: 'system:menu:delete'
          },
          {
            id: 25,
            url: null,
            name: '修改菜单',
            sort: null,
            type: 3,
            parentId: 4,
            permission: 'system:menu:edit'
          },
          {
            id: 26,
            url: null,
            name: '查询菜单',
            sort: null,
            type: 3,
            parentId: 4,
            permission: 'system:menu:query'
          }
        ]
      },
      {
        id: 10,
        url: '/main/system/role',
        name: '角色管理',
        sort: 101,
        type: 2,
        children: [
          {
            id: 27,
            url: null,
            name: '创建角色',
            sort: null,
            type: 3,
            parentId: 5,
            permission: 'system:role:create'
          },
          {
            id: 28,
            url: null,
            name: '删除角色',
            sort: null,
            type: 3,
            parentId: 5,
            permission: 'system:role:delete'
          },
          {
            id: 29,
            url: null,
            name: '修改角色',
            sort: null,
            type: 3,
            parentId: 5,
            permission: 'system:role:edit'
          },
          {
            id: 30,
            url: null,
            name: '查询角色',
            sort: null,
            type: 3,
            parentId: 5,
            permission: 'system:role:query'
          }
        ]
      }
    ]
  },
  {
    icon: 'Handbag',
    id: 3,
    name: '商品中心',
    sort: 3,
    type: 1,
    url: '/main/product',
    children: [
      {
        id: 11,
        url: '/main/product/category',
        name: '商品类别',
        sort: 20,
        type: 2,
        children: null,
        parentId: 9
      },
      {
        id: 12,
        url: '/main/product/goods',
        name: '商品信息',
        sort: 21,
        type: 2,
        children: null,
        parentId: 9
      }
    ]
  },
  {
    icon: 'ChatLineRound',
    id: 4,
    name: '随便聊聊',
    sort: 5,
    type: 1,
    url: '/main/story',
    children: [
      {
        id: 13,
        url: '/main/story/chat',
        name: '你的故事',
        sort: 30,
        type: 2,
        children: null,
        parentId: 70
      },
      {
        id: 14,
        url: '/main/story/list',
        name: '故事列表',
        sort: 31,
        type: 2,
        children: null,
        parentId: 70
      }
    ]
  }
]

export const departmentList = [
  {
    name: '营销部',
    id: '营销部'
  },
  {
    name: '人事部',
    id: '人事部'
  },
  {
    name: '客服部',
    id: '客服部'
  },
  {
    name: '运营部',
    id: '运营部'
  },
  {
    name: '研发部',
    id: '研发部'
  },
  {
    name: '总裁办',
    id: '总裁办'
  }
]

export const roleList = [
  {
    name: '普通用户',
    id: '普通用户'
  },
  {
    name: '管理员',
    id: '管理员'
  }
]

export const menuList = [
  {
    icon: 'Monitor',
    id: 1,
    name: '系统总览',
    sort: 1,
    type: 1,
    url: '/main/analysis',
    children: [
      {
        id: 5,
        url: '/main/analysis/overview',
        name: '核心技术',
        sort: 106,
        type: 2,
        children: null,
        parentId: 38
      },
      {
        id: 6,
        url: '/main/analysis/dashboard',
        name: '商品统计',
        sort: 107,
        type: 2,
        children: null,
        parentId: 38
      }
    ]
  },
  {
    icon: 'Setting',
    id: 2,
    name: '系统管理',
    sort: 2,
    type: 1,
    url: '/main/system',
    children: [
      {
        id: 7,
        url: '/main/system/user',
        name: '用户管理',
        sort: 100,
        type: 2,
        children: [
          {
            id: 15,
            url: null,
            name: '创建用户',
            sort: null,
            type: 3,
            parentId: 2,
            permission: 'system:users:create'
          },
          {
            id: 16,
            url: null,
            name: '删除用户',
            sort: null,
            type: 3,
            parentId: 2,
            permission: 'system:users:delete'
          },
          {
            id: 17,
            url: null,
            name: '修改用户',
            sort: null,
            type: 3,
            parentId: 2,
            permission: 'system:users:edit'
          },
          {
            id: 18,
            url: null,
            name: '查询用户',
            sort: null,
            type: 3,
            parentId: 2,
            permission: 'system:users:query'
          }
        ]
      },
      {
        id: 8,
        url: '/main/system/department',
        name: '部门管理',
        sort: 101,
        type: 2,
        children: [
          {
            id: 18,
            url: null,
            name: '创建部门',
            sort: null,
            type: 3,
            parentId: 3,
            permission: 'system:department:create'
          },
          {
            id: 20,
            url: null,
            name: '删除部门',
            sort: null,
            type: 3,
            parentId: 3,
            permission: 'system:department:delete'
          },
          {
            id: 21,
            url: null,
            name: '修改部门',
            sort: null,
            type: 3,
            parentId: 3,
            permission: 'system:department:edit'
          },
          {
            id: 22,
            url: null,
            name: '查询部门',
            sort: null,
            type: 3,
            parentId: 3,
            permission: 'system:department:query'
          }
        ]
      },
      {
        id: 9,
        url: '/main/system/menu',
        name: '菜单管理',
        sort: 102,
        type: 2,
        children: [
          {
            id: 23,
            url: null,
            name: '创建菜单',
            sort: null,
            type: 3,
            parentId: 4,
            permission: 'system:menu:create'
          },
          {
            id: 24,
            url: null,
            name: '删除菜单',
            sort: null,
            type: 3,
            parentId: 4,
            permission: 'system:menu:delete'
          },
          {
            id: 25,
            url: null,
            name: '修改菜单',
            sort: null,
            type: 3,
            parentId: 4,
            permission: 'system:menu:edit'
          },
          {
            id: 26,
            url: null,
            name: '查询菜单',
            sort: null,
            type: 3,
            parentId: 4,
            permission: 'system:menu:query'
          }
        ]
      },
      {
        id: 10,
        url: '/main/system/role',
        name: '角色管理',
        sort: 101,
        type: 2,
        children: [
          {
            id: 27,
            url: null,
            name: '创建角色',
            sort: null,
            type: 3,
            parentId: 5,
            permission: 'system:role:create'
          },
          {
            id: 28,
            url: null,
            name: '删除角色',
            sort: null,
            type: 3,
            parentId: 5,
            permission: 'system:role:delete'
          },
          {
            id: 29,
            url: null,
            name: '修改角色',
            sort: null,
            type: 3,
            parentId: 5,
            permission: 'system:role:edit'
          },
          {
            id: 30,
            url: null,
            name: '查询角色',
            sort: null,
            type: 3,
            parentId: 5,
            permission: 'system:role:query'
          }
        ]
      }
    ]
  },
  {
    icon: 'Handbag',
    id: 3,
    name: '商品中心',
    sort: 3,
    type: 1,
    url: '/main/product',
    children: [
      {
        id: 11,
        url: '/main/product/category',
        name: '商品类别',
        sort: 20,
        type: 2,
        children: null,
        parentId: 9
      },
      {
        id: 12,
        url: '/main/product/goods',
        name: '商品信息',
        sort: 21,
        type: 2,
        children: null,
        parentId: 9
      }
    ]
  },
  {
    icon: 'ChatLineRound',
    id: 4,
    name: '随便聊聊',
    sort: 5,
    type: 1,
    url: '/main/story',
    children: [
      {
        id: 13,
        url: '/main/story/chat',
        name: '你的故事',
        sort: 30,
        type: 2,
        children: null,
        parentId: 70
      },
      {
        id: 14,
        url: '/main/story/list',
        name: '故事列表',
        sort: 31,
        type: 2,
        children: null,
        parentId: 70
      }
    ]
  }
]
