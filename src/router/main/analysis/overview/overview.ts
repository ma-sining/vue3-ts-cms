const overview = () => import('@/views/main/analysis/overview/an-overview.vue')
export default {
  path: '/main/analysis/overview',
  name: 'overview',
  component: overview,
  children: []
}
