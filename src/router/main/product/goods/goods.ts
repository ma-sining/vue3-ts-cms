const goods = () => import('@/views/main/product/goods/pro-goods.vue')
export default {
  path: '/main/product/goods',
  name: 'goods',
  component: goods,
  children: []
}
