const category = () => import('@/views/main/product/category/pro-category.vue')
export default {
  path: '/main/product/category',
  name: 'category',
  component: category,
  children: []
}
