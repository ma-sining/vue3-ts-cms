const menu = () => import('@/views/main/system/menu/sys-menu.vue')
export default {
  path: '/main/system/menu',
  name: 'menu',
  component: menu,
  children: []
}
