const role = () => import('@/views/main/system/role/sys-role.vue')
export default {
  path: '/main/system/role',
  name: 'role',
  component: role,
  children: []
}
