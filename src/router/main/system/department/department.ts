const department = () =>
  import('@/views/main/system/department/sys-department.vue')
export default {
  path: '/main/system/department',
  name: 'department',
  component: department,
  children: []
}
