const chat = () => import('@/views/main/story/chat/story-chat.vue')
export default {
  path: '/main/story/chat',
  name: 'chat',
  component: chat,
  children: []
}
