import { Module } from 'vuex'

// import {
//   getCategoryGoodsCount,
//   getCategoryGoodsSale,
//   getCategoryGoodsFavor,
//   getAddressGoodsSale
// } from '@/service/main/analysis/dashboard'

import {
  goodsCategoryCount,
  goodsCategorySale,
  goodsCategoryFavor,
  goodsAddressSale
} from '@/assets/data/analysisData'

import { IDashboardState } from './types'
import { IRootState } from '../../types'

const dashboardModule: Module<IDashboardState, IRootState> = {
  namespaced: true,
  state() {
    return {
      categoryGoodsCount: [],
      categoryGoodsSale: [],
      categoryGoodsFavor: [],
      addressGoodsSale: []
    }
  },
  mutations: {
    changeCategoryGoodsCount(state, list) {
      state.categoryGoodsCount = list
    },
    changeCategoryGoodsSale(state, list) {
      state.categoryGoodsSale = list
    },
    changeCategoryGoodsFavor(state, list) {
      state.categoryGoodsFavor = list
    },
    changeAddressGoodsSale(state, list) {
      state.addressGoodsSale = list
    }
  },
  actions: {
    async getDashboardDataAction({ commit }) {
      // const categoryCountResult = await getCategoryGoodsCount()
      // commit('changeCategoryGoodsCount', categoryCountResult.data)
      // const categorySaleResult = await getCategoryGoodsSale()
      // commit('changeCategoryGoodsSale', categorySaleResult.data)
      // const categoryFavorResult = await getCategoryGoodsFavor()
      // commit('changeCategoryGoodsFavor', categoryFavorResult.data)
      // const addressGoodsResult = await getAddressGoodsSale()
      // commit('changeAddressGoodsSale', addressGoodsResult.data)

      commit('changeCategoryGoodsCount', goodsCategoryCount.data)

      commit('changeCategoryGoodsSale', goodsCategorySale.data)

      commit('changeCategoryGoodsFavor', goodsCategoryFavor.data)

      commit('changeAddressGoodsSale', goodsAddressSale.data)
    }
  }
}

export default dashboardModule
