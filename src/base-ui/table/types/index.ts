/**
 * 两种类型定义方式应该都是可行的propListType、testPropListType
 */
export interface propListType {
  prop?: string
  label: string
  minWidth?: string
  slotName?: string
}
export type testPropListType = {
  prop?: string
  label: string
  minWidth?: string
  slotName?: string
}[]

export interface ITable {
  title: string
  // propList: testPropListType
  propList: propListType[]
  showIndexColumn?: boolean
  showSelectColumn?: boolean
  childrenProps?: any
  showFooter?: boolean
}
