const debounce = (fn: any, delay: number) => {
  let timer: any
  return function (this: any, ...args: any[]) {
    const that = this
    clearTimeout(timer)
    timer = setTimeout(function () {
      fn.apply(that, args)
    }, delay)
  }
}

export default debounce
