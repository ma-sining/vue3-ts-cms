import { createApp } from 'vue'
import 'normalize.css'
import '@/assets/css/index.less'

import App from './App.vue'
import router from './router'
import store from './store'
// import maRequest from './service'
import { setupStore } from './store'
// import { AxiosRequestHeaders } from 'axios'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import { globalRegister } from './global'
import debounce from './utils/debounce'

//引入Elmessage和Elloading的css样式文件
import 'element-plus/theme-chalk/el-loading.css'
import 'element-plus/theme-chalk/el-message.css'

const app = createApp(App)

// 对Element 的内容区域或 SVGElement的边界框改变进行节流
const _ResizeObserver = window.ResizeObserver
window.ResizeObserver = class ResizeObserver extends _ResizeObserver {
  constructor(callback: any) {
    callback = debounce(callback, 16)
    super(callback)
  }
}

// 全局注册icon
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
// 全局注册(默认会将app传递给globalRegister)
app.use(globalRegister)
app.use(store)
setupStore()
app.use(router)

app.mount('#app')

// interface DataType {
//   data: any
//   returnCode: string
//   success: boolean
// }

// maRequest
//   .get<DataType>({
//     url: '/home/multidata',
//     showLoading: false,
//     headers: {} as AxiosRequestHeaders,
//     interceptors: {
//       requestInterceptor: (config) => {
//         return config
//       },
//       responseInterceptor: (res) => {
//         return res
//       }
//     }
//   })
//   .then((res) => {
//     console.log(res)
//   })
